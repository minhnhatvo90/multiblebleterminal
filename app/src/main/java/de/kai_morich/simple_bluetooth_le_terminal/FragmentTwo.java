package de.kai_morich.simple_bluetooth_le_terminal;


import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.kai_morich.simple_bluetooth_le_terminal.adapter.ButtonAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTwo extends Fragment {


    public FragmentTwo() {
        // Required empty public constructor
    }

    public static LineChart chart1;
    public static LineChart chart2;
    public static LinearLayout chart12;
    public static LineChart chart3;
    public static LineChart chart4;
    public static LinearLayout chart34;
    public static LineChart chart5;
    public static LinearLayout chart5logo;
    public static TextView chart1Temp;
    public static TextView chart2Temp;
    public static TextView chart3Temp;
    public static TextView chart4Temp;
    public static TextView chart5Temp;

    public static LinearLayout chart1Frame;
    public static LinearLayout chart2Frame;
    public static LinearLayout chart3Frame;
    public static LinearLayout chart4Frame;
    public static LinearLayout chart5Frame;

    private LinearLayout timeFrame;
    private LinearLayout limitFrame;
    private LinearLayout functionFrame;
    private LinearLayout applyFrame;
    private LinearLayout settingFrame;
    private LinearLayout rangeFrame;

    private EditText autoTime;
    private EditText upperLimit;
    private EditText a;
    private EditText b;
    private EditText c;
    private EditText d;
    private EditText e;
    private EditText upperRange;
    private EditText lowerRange;
    protected Typeface tfRegular;
    private Button button;
    private Button applyButton;
    private Button resetButton;
    private Button settingButton;

    int[] autoTimeValue = {1,1,1,1,1};
    int[] upperLimitValue = {40,40,40,40,40};
    public static double[] aValue = {0,0,0,0,0};
    public static double[] bValue = {0,0,0,0,0};
    public static double[] cValue = {0,0,0,0,0};
    public static double[] dValue = {1,1,1,1,1};
    public static double[] eValue = {0,0,0,0,0};
    public static int[] upperRangeValue = {0,0,0,0,0};
    public static int[] lowerRangeValue = {0,0,0,0,0};
    int currentChart = 0;
    boolean setting = false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_two, container, false);

        SharedPreferences settings = getActivity().getSharedPreferences("minhnhatvo90", 0);

        for(int i=0;i<5;i++)
        {
            autoTimeValue[i] = settings.getInt("autoTimeValue" + (i + 1), -1);
            if(autoTimeValue[i] == -1)
                autoTimeValue[i] = 1;
            upperLimitValue[i] = settings.getInt("upperLimitValue" + (i + 1), -1);
            if(upperLimitValue[i] == -1)
                upperLimitValue[i] = 40;
            try{
                aValue[i] = Double.parseDouble(settings.getString("aValue" + (i + 1), "-1"));
            } catch (final NumberFormatException e) {
                aValue[i] = -1;
            }
            try{
                bValue[i] = Double.parseDouble(settings.getString("bValue" + (i + 1), "-1"));
            } catch (final NumberFormatException e) {
                bValue[i] = -1;
            }
            try{
                cValue[i] = Double.parseDouble(settings.getString("cValue" + (i + 1), "-1"));
            } catch (final NumberFormatException e) {
                cValue[i] = -1;
            }
            try{
                dValue[i] = Double.parseDouble(settings.getString("dValue" + (i + 1), "-1"));
            } catch (final NumberFormatException e) {
                dValue[i] = -1;
            }
            try{
                eValue[i] = Double.parseDouble(settings.getString("eValue" + (i + 1), "-1"));
            } catch (final NumberFormatException e) {
                eValue[i] = -1;
            }
            try{
                upperRangeValue[i] = settings.getInt("upperRangeValue" + (i + 1), -1);
            } catch (final NumberFormatException e) {
                upperRangeValue[i] = -1;
            }
            try{
                lowerRangeValue[i] = settings.getInt("lowerRangeValue" + (i + 1), -1);
            } catch (final NumberFormatException e) {
                lowerRangeValue[i] = -1;
            }
            if(aValue[i] == -1)
                aValue[i] = 0;
            if(bValue[i] == -1)
                bValue[i] = 0;
            if(cValue[i] == -1)
                cValue[i] = 0;
            if(dValue[i] == -1)
                dValue[i] = 1;
            if(eValue[i] == -1)
                eValue[i] = 0;
            if(upperRangeValue[i] == -1)
                upperRangeValue[i] = 45;
            if(lowerRangeValue[i] == -1)
                lowerRangeValue[i] = 30;
        }

        tfRegular = Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Regular.ttf");

        chart1 = view.findViewById(R.id.chart1);
        chart2 = view.findViewById(R.id.chart2);
        chart12 = view.findViewById(R.id.chart12);
        chart3 = view.findViewById(R.id.chart3);
        chart4 = view.findViewById(R.id.chart4);
        chart34 = view.findViewById(R.id.chart34);
        chart5 = view.findViewById(R.id.chart5);
        chart5logo = view.findViewById(R.id.chart5logo);

        chart1.getDescription().setText("");
        chart2.getDescription().setText("");
        chart3.getDescription().setText("");
        chart4.getDescription().setText("");
        chart5.getDescription().setText("");

        chart1Frame = view.findViewById(R.id.chart1frame);
        chart2Frame = view.findViewById(R.id.chart2frame);
        chart3Frame = view.findViewById(R.id.chart3frame);
        chart4Frame = view.findViewById(R.id.chart4frame);
        chart5Frame = view.findViewById(R.id.chart5frame);

        chart1Temp = view.findViewById(R.id.chart1Temp);
        chart2Temp = view.findViewById(R.id.chart2Temp);
        chart3Temp = view.findViewById(R.id.chart3Temp);
        chart4Temp = view.findViewById(R.id.chart4Temp);
        chart5Temp = view.findViewById(R.id.chart5Temp);

        autoTime = view.findViewById(R.id.autoTime);
        upperLimit = view.findViewById(R.id.upperLimit);
        a = view.findViewById(R.id.a);
        b = view.findViewById(R.id.b);
        c = view.findViewById(R.id.c);
        d = view.findViewById(R.id.d);
        e = view.findViewById(R.id.e);
        upperRange = view.findViewById(R.id.upperRange);
        lowerRange = view.findViewById(R.id.lowerRange);

        timeFrame = view.findViewById(R.id.timeFrame);
        limitFrame = view.findViewById(R.id.limitFrame);
        functionFrame = view.findViewById(R.id.functionFrame);
        applyFrame = view.findViewById(R.id.applyFrame);
        settingFrame = view.findViewById(R.id.settingFrame);
        rangeFrame = view.findViewById(R.id.ramgeFrame);

        initChart(chart1, 1);
        initChart(chart2, 2) ;
        initChart(chart3, 3);
        initChart(chart4, 4);
        initChart(chart5, 5);

        feedMultiple(1);
        feedMultiple(2);
        feedMultiple(3);
        feedMultiple(4);
        feedMultiple(5);

        button = view.findViewById(R.id.button2);
        applyButton = view.findViewById(R.id.applyButton);
        resetButton = view.findViewById(R.id.resetButton);
        settingButton = view.findViewById(R.id.settingBtn);

        button.setVisibility(View.GONE);

        chart1.setOnChartValueSelectedListener(new OnChartValueSelectedListener()
        {
            @Override
            public void onValueSelected(Entry e, Highlight h)
            {
                chart1Touch();
            }

            @Override
            public void onNothingSelected()
            {
                chart1Touch();
            }
        });

        chart2.setOnChartValueSelectedListener(new OnChartValueSelectedListener()
        {
            @Override
            public void onValueSelected(Entry e, Highlight h)
            {
                chart2Touch();
            }

            @Override
            public void onNothingSelected()
            {
                chart2Touch();
            }
        });

        chart3.setOnChartValueSelectedListener(new OnChartValueSelectedListener()
        {
            @Override
            public void onValueSelected(Entry e, Highlight h)
            {
                chart3Touch();
            }

            @Override
            public void onNothingSelected()
            {
                chart3Touch();
            }
        });

        chart4.setOnChartValueSelectedListener(new OnChartValueSelectedListener()
        {
            @Override
            public void onValueSelected(Entry e, Highlight h)
            {
                chart4Touch();
            }

            @Override
            public void onNothingSelected()
            {
                chart4Touch();
            }
        });

        chart5.setOnChartValueSelectedListener(new OnChartValueSelectedListener()
        {
            @Override
            public void onValueSelected(Entry e, Highlight h)
            {
                chart5Touch();
            }

            @Override
            public void onNothingSelected()
            {
                chart5Touch();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                button.setVisibility(View.GONE);
                chart12.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1, (float)0.33));
                chart34.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1, (float)0.33));
                chart5logo.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1, (float)0.33));
                chart1Frame.setLayoutParams(new LinearLayout.LayoutParams(1, LinearLayout.LayoutParams.MATCH_PARENT, (float)0.5));
                chart2Frame.setLayoutParams(new LinearLayout.LayoutParams(1, LinearLayout.LayoutParams.MATCH_PARENT, (float)0.5));
                chart3Frame.setLayoutParams(new LinearLayout.LayoutParams(1, LinearLayout.LayoutParams.MATCH_PARENT, (float)0.5));
                chart4Frame.setLayoutParams(new LinearLayout.LayoutParams(1, LinearLayout.LayoutParams.MATCH_PARENT, (float)0.5));
                chart5Frame.setLayoutParams(new LinearLayout.LayoutParams(1, LinearLayout.LayoutParams.MATCH_PARENT, (float)0.5));
                settingFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
                timeFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
                limitFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
                functionFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
                applyFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
                rangeFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
                setting = false;
            }
        });

        applyButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int tempInt = 0;
                try {
                    tempInt = Integer.parseInt(autoTime.getText().toString());
                } catch(NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
                autoTimeValue[currentChart - 1] = tempInt;
                try {
                    tempInt = Integer.parseInt(upperLimit.getText().toString());
                } catch(NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
                upperLimitValue[currentChart - 1] = tempInt;

                double tempFloat = 0;
                try {
                    tempFloat = Double.parseDouble(a.getText().toString());
                } catch(NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
                aValue[currentChart - 1] = tempFloat;
                try {
                    tempFloat = Double.parseDouble(b.getText().toString());
                } catch(NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
                bValue[currentChart - 1] = tempFloat;
                try {
                    tempFloat = Double.parseDouble(c.getText().toString());
                } catch(NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
                cValue[currentChart - 1] = tempFloat;
                try {
                    tempFloat = Double.parseDouble(d.getText().toString());
                } catch(NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
                dValue[currentChart - 1] = tempFloat;
                try {
                    tempFloat = Double.parseDouble(e.getText().toString());
                } catch(NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
                eValue[currentChart - 1] = tempFloat;
                try {
                    tempInt = Integer.parseInt(upperRange.getText().toString());
                } catch(NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
                upperRangeValue[currentChart - 1] = tempInt;
                try {
                    tempInt = Integer.parseInt(lowerRange.getText().toString());
                } catch(NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
                lowerRangeValue[currentChart - 1] = tempInt;

                LimitLine ll1 = new LimitLine((float) upperLimitValue[currentChart - 1], "High Temp");
                ll1.setLineWidth(2f);
                ll1.enableDashedLine(10f, 10f, 0f);
                ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
                ll1.setTextSize(10f);
                ll1.setTypeface(tfRegular);

                LimitLine ll2 = new LimitLine(lowerRangeValue[currentChart - 1], "");
                ll2.setLineWidth(0.3f);
                ll2.setLineColor(Color.BLACK);
                ll2.setTypeface(tfRegular);

                if(currentChart == 1)
                {
                    YAxis leftAxis = chart1.getAxisLeft();
                    leftAxis.removeAllLimitLines();
                    leftAxis.addLimitLine(ll1);
                    leftAxis.addLimitLine(ll2);
                    leftAxis.setAxisMaximum(upperRangeValue[currentChart - 1]);
                    leftAxis.setAxisMinimum(lowerRangeValue[currentChart - 1]);
                }
                else if(currentChart == 2)
                {
                    YAxis leftAxis = chart2.getAxisLeft();
                    leftAxis.removeAllLimitLines();
                    leftAxis.addLimitLine(ll1);
                    leftAxis.addLimitLine(ll2);
                    leftAxis.setAxisMaximum(upperRangeValue[currentChart - 1]);
                    leftAxis.setAxisMinimum(lowerRangeValue[currentChart - 1]);
                }
                else if(currentChart == 3)
                {
                    YAxis leftAxis = chart3.getAxisLeft();
                    leftAxis.removeAllLimitLines();
                    leftAxis.addLimitLine(ll1);
                    leftAxis.addLimitLine(ll2);
                    leftAxis.setAxisMaximum(upperRangeValue[currentChart - 1]);
                    leftAxis.setAxisMinimum(lowerRangeValue[currentChart - 1]);
                }
                else if(currentChart == 4)
                {
                    YAxis leftAxis = chart4.getAxisLeft();
                    leftAxis.removeAllLimitLines();
                    leftAxis.addLimitLine(ll1);
                    leftAxis.addLimitLine(ll2);
                    leftAxis.setAxisMaximum(upperRangeValue[currentChart - 1]);
                    leftAxis.setAxisMinimum(lowerRangeValue[currentChart - 1]);
                }
                else if(currentChart == 5)
                {
                    YAxis leftAxis = chart5.getAxisLeft();
                    leftAxis.removeAllLimitLines();
                    leftAxis.addLimitLine(ll1);
                    leftAxis.addLimitLine(ll2);
                    leftAxis.setAxisMaximum(upperRangeValue[currentChart - 1]);
                    leftAxis.setAxisMinimum(lowerRangeValue[currentChart - 1]);
                }

                hideAllKeyboard();
            }
        });
        resetButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(currentChart == 1)
                {
                    timeList1.clear();
                    chart1.invalidate();
                    initChart(chart1, 1);
                }
                else if(currentChart == 2)
                {
                    timeList2.clear();
                    chart2.invalidate();
                    initChart(chart2, 2);
                }
                else if(currentChart == 3)
                {
                    timeList3.clear();
                    chart3.invalidate();
                    initChart(chart3, 3);
                }
                else if(currentChart == 4)
                {
                    timeList4.clear();
                    chart4.invalidate();
                    initChart(chart4,4);
                }
                else if(currentChart == 5)
                {
                    timeList5.clear();
                    chart5.invalidate();
                    initChart(chart5,5);
                }
            }
        });
        settingButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(!setting) {
                    timeFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    limitFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    functionFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    applyFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    rangeFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                }
                else
                {
                    timeFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
                    limitFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
                    functionFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
                    applyFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
                    rangeFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
                }
                setting = !setting;
            }
        });
        // Inflate the layout for this fragment
        return view;
    }

    void chart1Touch()
    {
        settingFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        chart12.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        chart1Frame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        button.setVisibility(View.VISIBLE);
        currentChart = 1;
        hideAllKeyboard();
    }

    void chart2Touch()
    {
        settingFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        chart12.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        chart2Frame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        button.setVisibility(View.VISIBLE);
        currentChart = 2;
        hideAllKeyboard();
    }

    void chart3Touch()
    {
        settingFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        chart12.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
        chart34.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        chart5logo.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
        chart3Frame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        button.setVisibility(View.VISIBLE);
        currentChart = 3;
        hideAllKeyboard();
    }

    void chart4Touch()
    {
        settingFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        chart12.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
        chart34.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        chart5logo.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
        chart4Frame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        button.setVisibility(View.VISIBLE);
        currentChart = 4;
        hideAllKeyboard();
    }

    void chart5Touch()
    {
        settingFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        chart12.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
        chart34.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
        chart5logo.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        chart5Frame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        button.setVisibility(View.VISIBLE);
        currentChart = 5;
        hideAllKeyboard();
    }

    void hideAllKeyboard()
    {
        try {
            autoTime.setText(String.format("%d", autoTimeValue[currentChart - 1]));
            upperLimit.setText(String.format("%d", upperLimitValue[currentChart - 1]));
            upperRange.setText(String.format("%d", upperRangeValue[currentChart - 1]));
            lowerRange.setText(String.format("%d", lowerRangeValue[currentChart - 1]));
            DecimalFormat df = new DecimalFormat("0.000000000000000");
            a.setText(df.format(aValue[currentChart - 1]));
            b.setText(df.format(bValue[currentChart - 1]));
            c.setText(df.format(cValue[currentChart - 1]));
            d.setText(df.format(dValue[currentChart - 1]));
            e.setText(df.format(eValue[currentChart - 1]));

            SharedPreferences settings = getActivity().getSharedPreferences("minhnhatvo90", 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("autoTimeValue" + currentChart, autoTimeValue[currentChart - 1]);
            editor.putInt("upperLimitValue" + currentChart, upperLimitValue[currentChart - 1]);
            editor.putInt("upperRangeValue" + currentChart, upperRangeValue[currentChart - 1]);
            editor.putInt("lowerRangeValue" + currentChart, lowerRangeValue[currentChart - 1]);
            editor.putString("aValue" + currentChart, String.valueOf(aValue[currentChart - 1]));
            editor.putString("bValue" + currentChart, String.valueOf(bValue[currentChart - 1]));
            editor.putString("cValue" + currentChart, String.valueOf(cValue[currentChart - 1]));
            editor.putString("dValue" + currentChart, String.valueOf(dValue[currentChart - 1]));
            editor.putString("eValue" + currentChart, String.valueOf(eValue[currentChart - 1]));
            editor.apply();

            autoTime.clearFocus();
            upperLimit.clearFocus();
            upperRange.clearFocus();
            lowerRange.clearFocus();
            a.clearFocus();
            b.clearFocus();
            c.clearFocus();
            d.clearFocus();
            e.clearFocus();
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
        catch (Exception ex)
        {

        }
    }

    void initChart(LineChart chart, int chartNumber)
    {
        // enable description text
        chart.getDescription().setEnabled(true);
        // enable touch gestures
        chart.setTouchEnabled(true);

        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setDrawGridBackground(false);

        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(true);

        // set an alternative background color
        chart.setBackgroundColor(Color.WHITE);

        LineData data = new LineData();
        data.setValueTextColor(Color.LTGRAY);

        // add empty data
        chart.setData(data);
        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();
        l.setEnabled(false);
        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);
        l.setTextColor(Color.LTGRAY);

        XAxis xl = chart.getXAxis();
        xl.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                if(value %60 == 0)
                    return String.format("%02d", (int)value/60/60)+ ":" + String.format("%02d", ((int)(value/60))%60);
                else
                    return "";
            }
        });
        xl.setTextColor(Color.BLACK);
        xl.setDrawGridLines(false);
        xl.setTextSize(15);
        //xl.enableGridDashedLine(10f, 10f, 0f);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setTextColor(Color.BLACK);
        leftAxis.setDrawGridLines(false);
        leftAxis.setAxisMaximum(upperRangeValue[chartNumber - 1]);
        leftAxis.setAxisMinimum(lowerRangeValue[chartNumber - 1]);
        leftAxis.setTextSize(15);
        ///leftAxis.enableGridDashedLine(10f, 10f, 0f);

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(true);
        rightAxis.setDrawAxisLine(true);
        rightAxis.setDrawGridLines(false);
        rightAxis.setTextColor(Color.TRANSPARENT);
        rightAxis.setTextSize(0);

        {   // // Create Limit Lines // //
            LimitLine ll1 = new LimitLine(upperLimitValue[chartNumber - 1], "High Temp");
            ll1.setLineWidth(2f);
            ll1.enableDashedLine(10f, 10f, 0f);
            ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
            ll1.setTextSize(10f);
            ll1.setTypeface(tfRegular);

            LimitLine ll2 = new LimitLine(lowerRangeValue[chartNumber - 1], "");
            ll2.setLineWidth(0.3f);
            ll2.setLineColor(Color.BLACK);
            ll2.setTypeface(tfRegular);

            // draw limit lines behind data instead of on top
            leftAxis.setDrawLimitLinesBehindData(true);
            xl.setDrawLimitLinesBehindData(true);

            // add limit lines
            leftAxis.addLimitLine(ll1);
            leftAxis.addLimitLine(ll2);
            //xAxis.addLimitLine(llXAxis);
        }
    }

    public static LineDataSet createSet() {

        LineDataSet set = new LineDataSet(null, "Temperature");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(ColorTemplate.getHoloBlue());
        set.setDrawCircles(false);
        set.setLineWidth(1f);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setHighLightColor(Color.TRANSPARENT);
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(9f);
        set.setDrawValues(false);
        return set;
    }

    private Thread thread;

    public static void addEntry(double temp, long time, int chartNumber) {
        LineData data;

        if(chartNumber == 1)
            data = chart1.getData();
        else if(chartNumber == 2)
            data = chart2.getData();
        else if(chartNumber == 3)
            data = chart3.getData();
        else if(chartNumber == 4)
            data = chart4.getData();
        else
            data = chart5.getData();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);
            // set.addEntry(...); // can be called as well

            if (set == null) {
                set = createSet();
                data.addDataSet(set);
            }

            temp = temp * temp * temp * temp * aValue[chartNumber - 1] +temp * temp * temp * bValue[chartNumber - 1] +temp * temp * cValue[chartNumber - 1] +temp * dValue[chartNumber - 1] +
                    eValue[chartNumber - 1];
            data.addEntry(new Entry(time, (float)temp), 0);
            data.notifyDataChanged();
            DecimalFormat df = new DecimalFormat("0.0");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            if(chartNumber == 1)
            {
                timeList1.add(currentDateandTime);
                chart1.notifyDataSetChanged();
                chart1.setVisibleXRangeMaximum(600);
                chart1.moveViewToX(time);
                if(temp >= lowerRangeValue[0] && temp <= upperRangeValue[0])
                    chart1Temp.setText(df.format(temp) +"°C");
            }
            else if(chartNumber == 2)
            {
                timeList2.add(currentDateandTime);
                chart2.notifyDataSetChanged();
                chart2.setVisibleXRangeMaximum(600);
                chart2.moveViewToX(time);
                if(temp >= lowerRangeValue[1] && temp <= upperRangeValue[1])
                    chart2Temp.setText(df.format(temp)+"°C");
            }
            else if(chartNumber == 3)
            {
                timeList3.add(currentDateandTime);
                chart3.notifyDataSetChanged();
                chart3.setVisibleXRangeMaximum(600);
                chart3.moveViewToX(time);
                if(temp >= lowerRangeValue[2] && temp <= upperRangeValue[2])
                    chart3Temp.setText(df.format(temp)+"°C");
            }
            else if(chartNumber == 4)
            {
                timeList4.add(currentDateandTime);
                chart4.notifyDataSetChanged();
                chart4.setVisibleXRangeMaximum(600);
                chart4.moveViewToX(time);
                if(temp >= lowerRangeValue[3] && temp <= upperRangeValue[3])
                    chart4Temp.setText(df.format(temp)+"°C");
            }
            else
            {
                timeList5.add(currentDateandTime);
                chart5.notifyDataSetChanged();
                chart5.setVisibleXRangeMaximum(600);
                chart5.moveViewToX(time);
                if(temp >= lowerRangeValue[4] && temp <= upperRangeValue[4])
                    chart5Temp.setText(df.format(temp)+"°C");
            }

        }
    }

    private void feedMultiple(int chartNumber) {

        if (thread != null)
            thread.interrupt();

        final Runnable runnable = new Runnable() {

            @Override
            public void run() {
                if(chartNumber == 1)
                {
                    if(TerminalFragment1.service != null) {
                        String newline = TextUtil.newline_crlf;
                        byte[] data;
                        data = ("v" + newline).getBytes();
                        try {
                            TerminalFragment1.service.write(data);
                        } catch (Exception e) {
                        }

                    }
                }
                else if(chartNumber == 2)
                {
                    if(TerminalFragment2.service != null) {
                        String newline = TextUtil.newline_crlf;
                        byte[] data;
                        data = ("v" + newline).getBytes();
                        try {
                            TerminalFragment2.service.write(data);
                        } catch (Exception e) {
                        }

                    }
                }
                else if(chartNumber == 3)
                {
                    if(TerminalFragment3.service != null) {
                        String newline = TextUtil.newline_crlf;
                        byte[] data;
                        data = ("v" + newline).getBytes();
                        try {
                            TerminalFragment3.service.write(data);
                        } catch (Exception e) {
                        }
                    }
                }
                else if(chartNumber == 4)
                {
                    if(TerminalFragment4.service != null) {
                        String newline = TextUtil.newline_crlf;
                        byte[] data;
                        data = ("v" + newline).getBytes();
                        try {
                            TerminalFragment4.service.write(data);
                        } catch (Exception e) {
                        }
                    }
                }
                else
                {
                    if(TerminalFragment5.service != null) {
                        String newline = TextUtil.newline_crlf;
                        byte[] data;
                        data = ("v" + newline).getBytes();
                        try {
                            TerminalFragment5.service.write(data);
                        } catch (Exception e) {
                        }
                    }
                }
                //addEntry(chart);
            }
        };

        thread = new Thread(new Runnable() {

            @Override
            public void run() {
                while(true) {

                    // Don't generate garbage runnables inside the loop.
                    getActivity().runOnUiThread(runnable);

                    try {
                        Thread.sleep(autoTimeValue[chartNumber - 1] * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        thread.start();
    }

    public static List<String> timeList1 = new ArrayList<>();
    public static List<String> timeList2 = new ArrayList<>();
    public static List<String> timeList3 = new ArrayList<>();
    public static List<String> timeList4 = new ArrayList<>();
    public static List<String> timeList5 = new ArrayList<>();
}
