package de.kai_morich.simple_bluetooth_le_terminal;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import de.kai_morich.simple_bluetooth_le_terminal.adapter.ButtonAdapter;

public class MacroActivity extends AppCompatActivity {

    private EditText name;
    private EditText value;
    private RadioButton textRadio;
    private RadioButton hexRadio;
    private RadioButton sendRadio;
    private RadioButton insertRadio;
    private TextUtil.HexWatcher hexWatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_macro);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        name = findViewById(R.id.nameEditText);
        value = findViewById(R.id.valueEditText);
        textRadio = findViewById(R.id.textRadio);
        hexRadio = findViewById(R.id.hexRadio);
        sendRadio = findViewById(R.id.sendRadio);
        insertRadio = findViewById(R.id.insertRadio);

        name.setText(ButtonAdapter.name);
        value.setText(ButtonAdapter.value);

        if(ButtonAdapter.mode == 0)
        {
            textRadio.setChecked(true);
        }
        else
        {
            hexRadio.setChecked(true);
        }

        if(ButtonAdapter.action == 0)
        {
            sendRadio.setChecked(true);
        }
        else
        {
            insertRadio.setChecked(true);
        }

        hexWatcher = new TextUtil.HexWatcher(value);
        hexWatcher.enable(hexRadio.isChecked());
        value.addTextChangedListener(hexWatcher);
        value.setHint(hexRadio.isChecked() ? "HEX mode" : "");

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.modeGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                value.setText("");
                hexWatcher.enable(hexRadio.isChecked());
                value.setHint(hexRadio.isChecked() ? "HEX mode" : "");
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Edit Macro");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_macro, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.checkmark:
                SharedPreferences settings = getApplicationContext().getSharedPreferences("minhnhatvo90", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("name" + ButtonAdapter.cmdPos, String.valueOf(name.getText()));
                editor.putString("value" + ButtonAdapter.cmdPos, String.valueOf(value.getText()));
                editor.putInt("mode" + ButtonAdapter.cmdPos, (textRadio.isChecked() ? 0 : 1));
                editor.putInt("action" + ButtonAdapter.cmdPos, (sendRadio.isChecked() ? 0 : 1));
                editor.apply();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
