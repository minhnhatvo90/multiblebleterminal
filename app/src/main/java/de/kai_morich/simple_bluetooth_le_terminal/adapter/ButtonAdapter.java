package de.kai_morich.simple_bluetooth_le_terminal.adapter;

import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import de.kai_morich.simple_bluetooth_le_terminal.DevicesFragment;
import de.kai_morich.simple_bluetooth_le_terminal.MacroActivity;
import de.kai_morich.simple_bluetooth_le_terminal.TerminalFragment1;
import de.kai_morich.simple_bluetooth_le_terminal.TerminalFragment2;
import de.kai_morich.simple_bluetooth_le_terminal.TerminalFragment3;
import de.kai_morich.simple_bluetooth_le_terminal.TerminalFragment4;
import de.kai_morich.simple_bluetooth_le_terminal.TerminalFragment5;

public class ButtonAdapter extends BaseAdapter {
    private Context mContext;
    private int btn_id;
    private int total_btns = 10;

    public ButtonAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return total_btns;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public static int cmdPos = 0;
    public static String name = "";
    public static String value = "";
    public static int mode = 0;
    public static int action = 0;

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup)
    {
        Button btn;
        SharedPreferences settings = mContext.getSharedPreferences("minhnhatvo90", 0);

        name = settings.getString("name" + (i + 1), "");

        if (view == null) {
            btn = new Button(mContext);
            if(name == "")
                btn.setText("M" + (i + 1));
            else
                btn.setText(name);
        } else {
            btn = (Button) view;
        }

        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(DevicesFragment.index == 0) {
                    String value1 = settings.getString("value" + (i + 1), "");
                    int mode1 = settings.getInt("mode" + (i + 1), 0);
                    int action1 = settings.getInt("action" + (i + 1), 0);
                    TerminalFragment1.hexEnabled = (mode1 == 1);
                    TerminalFragment1.hexMode.setChecked(mode1 == 1);
                    TerminalFragment1.sendText.setText(value1);
                    if(action1 == 0)
                    {
                        TerminalFragment1.send(value1);
                    }
                }
                else if(DevicesFragment.index == 1) {
                    String value1 = settings.getString("value" + (i + 1), "");
                    int mode1 = settings.getInt("mode" + (i + 1), 0);
                    int action1 = settings.getInt("action" + (i + 1), 0);
                    TerminalFragment2.hexEnabled = (mode1 == 1);
                    TerminalFragment2.hexMode.setChecked(mode1 == 1);
                    TerminalFragment2.sendText.setText(value1);
                    if(action1 == 0)
                    {
                        TerminalFragment2.send(value1);
                    }
                }
                else if(DevicesFragment.index == 2) {
                    String value1 = settings.getString("value" + (i + 1), "");
                    int mode1 = settings.getInt("mode" + (i + 1), 0);
                    int action1 = settings.getInt("action" + (i + 1), 0);
                    TerminalFragment3.hexEnabled = (mode1 == 1);
                    TerminalFragment3.hexMode.setChecked(mode1 == 1);
                    TerminalFragment3.sendText.setText(value1);
                    if(action1 == 0)
                    {
                        TerminalFragment3.send(value1);
                    }
                }
                else if(DevicesFragment.index == 3) {
                    String value1 = settings.getString("value" + (i + 1), "");
                    int mode1 = settings.getInt("mode" + (i + 1), 0);
                    int action1 = settings.getInt("action" + (i + 1), 0);
                    TerminalFragment4.hexEnabled = (mode1 == 1);
                    TerminalFragment4.hexMode.setChecked(mode1 == 1);
                    TerminalFragment4.sendText.setText(value1);
                    if(action1 == 0)
                    {
                        TerminalFragment4.send(value1);
                    }
                }
                else if(DevicesFragment.index == 4) {
                    String value1 = settings.getString("value" + (i + 1), "");
                    int mode1 = settings.getInt("mode" + (i + 1), 0);
                    int action1 = settings.getInt("action" + (i + 1), 0);
                    TerminalFragment5.hexEnabled = (mode1 == 1);
                    TerminalFragment5.hexMode.setChecked(mode1 == 1);
                    TerminalFragment5.sendText.setText(value1);
                    if(action1 == 0)
                    {
                        TerminalFragment5.send(value1);
                    }
                }
                //Toast.makeText(v.getContext(), "Button #" + (i + 1), Toast.LENGTH_SHORT).show();
            }
        });

        btn.setOnLongClickListener(v -> {
            cmdPos = i + 1;
            name = settings.getString("name" + (i + 1), "");
            if(name == "")
                name = ("M" + (i + 1));
            value = settings.getString("value" + (i + 1), "");
            mode = settings.getInt("mode" + (i + 1), 0);
            action = settings.getInt("action" + (i + 1), 0);

            Intent myIntent = new Intent(mContext, MacroActivity.class);
            mContext.startActivity(myIntent);
            return false;
        });

        return btn;
    }
}
