package de.kai_morich.simple_bluetooth_le_terminal;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import de.kai_morich.simple_bluetooth_le_terminal.DevicesFragment;
import de.kai_morich.simple_bluetooth_le_terminal.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentOne extends Fragment {

    public static RelativeLayout fragment1;
    public static RelativeLayout fragment2;
    public static RelativeLayout fragment3;
    public static RelativeLayout fragment4;
    public static RelativeLayout fragment5;

    public FragmentOne() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_one, container, false);

        fragment1 = view.findViewById(R.id.fragment1);
        fragment2 = view.findViewById(R.id.fragment2);
        fragment3 = view.findViewById(R.id.fragment3);
        fragment4 = view.findViewById(R.id.fragment4);
        fragment5 = view.findViewById(R.id.fragment5);

        if (savedInstanceState == null)
            getFragmentManager().beginTransaction().add(R.id.fragment, new DevicesFragment(), "devices").commit();

        // Inflate the layout for this fragment
        return view;


    }

}
