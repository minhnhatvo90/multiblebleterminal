package de.kai_morich.simple_bluetooth_le_terminal;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * show list of BLE devices
 */
public class
DevicesFragment extends ListFragment {

    private enum ScanState { NONE, LESCAN, DISCOVERY, DISCOVERY_FINISHED }
    private ScanState                       scanState = ScanState.NONE;
    private static final long               LESCAN_PERIOD = 10000; // similar to bluetoothAdapter.startDiscovery
    private Handler                         leScanStopHandler = new Handler();
    private BluetoothAdapter.LeScanCallback leScanCallback;
    private BroadcastReceiver               discoveryBroadcastReceiver;
    private IntentFilter                    discoveryIntentFilter;

    private Menu                            menu;
    private BluetoothAdapter                bluetoothAdapter;
    private ArrayList<BluetoothDevice>      listItems = new ArrayList<>();
    private ArrayAdapter<BluetoothDevice>   listAdapter;

    public DevicesFragment() {
        leScanCallback = (device, rssi, scanRecord) -> {
            if(device != null && getActivity() != null) {
                getActivity().runOnUiThread(() -> { updateScan(device); });
            }
        };
        discoveryBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(BluetoothDevice.ACTION_FOUND)) {
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if(device.getType() != BluetoothDevice.DEVICE_TYPE_CLASSIC && getActivity() != null) {
                        getActivity().runOnUiThread(() -> updateScan(device));
                    }
                }
                if(intent.getAction().equals((BluetoothAdapter.ACTION_DISCOVERY_FINISHED))) {
                    scanState = ScanState.DISCOVERY_FINISHED; // don't cancel again
                    stopScan();
                }
            }
        };
        discoveryIntentFilter = new IntentFilter();
        discoveryIntentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        discoveryIntentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if(getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH))
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        listAdapter = new ArrayAdapter<BluetoothDevice>(getActivity(), 0, listItems) {
            @Override
            public View getView(int position, View view, ViewGroup parent) {
                BluetoothDevice device = listItems.get(position);
                if (view == null)
                    view = getActivity().getLayoutInflater().inflate(R.layout.device_list_item, parent, false);
                TextView text1 = view.findViewById(R.id.text1);
                TextView text2 = view.findViewById(R.id.text2);

                if (position == mSelectedItem - 1) {
                    LinearLayout frame = view.findViewById(R.id.frame);
                    frame.setBackgroundColor(Color.GREEN);
                }
                else
                {
                    LinearLayout frame = view.findViewById(R.id.frame);
                    frame.setBackgroundColor(Color.TRANSPARENT);
                }
                if(device.getName() == null || device.getName().isEmpty())
                    text1.setText("<unnamed>");
                else
                    text1.setText(device.getName());
                text2.setText(device.getAddress());
                return view;
            }
        };
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListAdapter(null);
        View header = getActivity().getLayoutInflater().inflate(R.layout.device_list_header, null, false);
        getListView().addHeaderView(header, null, false);
        setEmptyText("initializing...");
        ((TextView) getListView().getEmptyView()).setTextSize(18);
        setListAdapter(listAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_devices, menu);
        this.menu = menu;
        if (bluetoothAdapter == null) {
            menu.findItem(R.id.bt_settings).setEnabled(false);
            menu.findItem(R.id.checkmark).setEnabled(false);
        } else if(!bluetoothAdapter.isEnabled()) {
            menu.findItem(R.id.checkmark).setEnabled(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(discoveryBroadcastReceiver, discoveryIntentFilter);
        if(bluetoothAdapter == null) {
            setEmptyText("<bluetooth LE not supported>");
        } else if(!bluetoothAdapter.isEnabled()) {
            setEmptyText("<bluetooth is disabled>");
            if (menu != null) {
                listItems.clear();
                listAdapter.notifyDataSetChanged();
                menu.findItem(R.id.checkmark).setEnabled(false);
            }
        } else {
            setEmptyText("<use SCAN to refresh devices>");
            if (menu != null)
                menu.findItem(R.id.checkmark).setEnabled(true);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopScan();
        getActivity().unregisterReceiver(discoveryBroadcastReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        menu = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.checkmark) {
            startScan();
            return true;
        } else if (id == R.id.ble_scan_stop) {
            stopScan();
            return true;
        } else if (id == R.id.bt_settings) {
            Intent intent = new Intent();
            intent.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
            startActivity(intent);
            return true;
        }else if (id == R.id.export) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
            }

            String str1 = "";
            File folder1 = new File(String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)));
            if (folder1.exists()) {
                str1 = folder1.toString() + File.separator;
            }
            try {
                String content = "";
                for (int i = 0; i < FragmentTwo.chart1.getData().getDataSets().get(0).getEntryCount(); i++) {
                    String data = String.valueOf(FragmentTwo.chart1.getData().getDataSets().get(0).getEntryForIndex(i).getY());
                    content += FragmentTwo.timeList1.get(i) + ";" + data + "\n";
                }
                createTextFile(content, "F5DD.txt", str1);
            }
            catch (Exception ex)
            {

            }

            try {
                String content = "";
                for (int i = 0; i < FragmentTwo.chart2.getData().getDataSets().get(0).getEntryCount(); i++) {
                    String data = String.valueOf(FragmentTwo.chart2.getData().getDataSets().get(0).getEntryForIndex(i).getY());
                    content += FragmentTwo.timeList2.get(i) + ";" + data + "\n";
                }
                createTextFile(content, "BEE9.txt", str1);
            }
            catch (Exception ex)
            {

            }

            try {
                String content = "";
                for (int i = 0; i < FragmentTwo.chart3.getData().getDataSets().get(0).getEntryCount(); i++) {
                    String data = String.valueOf(FragmentTwo.chart3.getData().getDataSets().get(0).getEntryForIndex(i).getY());
                    content += FragmentTwo.timeList3.get(i) + ";" + data + "\n";
                }
                createTextFile(content, "chart3.txt", str1);
            }
            catch (Exception ex)
            {

            }

            try {
                String content = "";
                for (int i = 0; i < FragmentTwo.chart4.getData().getDataSets().get(0).getEntryCount(); i++) {
                    String data = String.valueOf(FragmentTwo.chart4.getData().getDataSets().get(0).getEntryForIndex(i).getY());
                    content += FragmentTwo.timeList4.get(i) + ";" + data + "\n";
                }
                createTextFile(content, "chart4.txt", str1);
            }
            catch (Exception ex)
            {

            }

            try {
                String content = "";
                for (int i = 0; i < FragmentTwo.chart5.getData().getDataSets().get(0).getEntryCount(); i++) {
                    String data = String.valueOf(FragmentTwo.chart5.getData().getDataSets().get(0).getEntryForIndex(i).getY());
                    content += FragmentTwo.timeList5.get(i) + ";" + data + "\n";
                }
                createTextFile(content, "chart5.txt", str1);
            }
            catch (Exception ex)
            {

            }

            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public static void createTextFile(String sBody, String FileName, String Where) {
        try {
            File gpxfile = new File(Where, FileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("StaticFieldLeak") // AsyncTask needs reference to this fragment
    private void startScan() {
        if(scanState != ScanState.NONE)
            return;
        scanState = ScanState.LESCAN;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                scanState = ScanState.NONE;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.location_permission_title);
                builder.setMessage(R.string.location_permission_message);
                builder.setPositiveButton(android.R.string.ok,
                        (dialog, which) -> requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0));
                builder.show();
                return;
            }
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            boolean         locationEnabled = false;
            try {
                locationEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } catch(Exception ignored) {}
            try {
                locationEnabled |= locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            } catch(Exception ignored) {}
            if(!locationEnabled)
                scanState = ScanState.DISCOVERY;
            // Starting with Android 6.0 a bluetooth scan requires ACCESS_COARSE_LOCATION permission, but that's not all!
            // LESCAN also needs enabled 'location services', whereas DISCOVERY works without.
            // Most users think of GPS as 'location service', but it includes more, as we see here.
            // Instead of asking the user to enable something they consider unrelated,
            // we fall back to the older API that scans for bluetooth classic _and_ LE
            // sometimes the older API returns less results or slower
        }
        //listItems.clear();
        listAdapter.notifyDataSetChanged();
        setEmptyText("<scanning...>");
        menu.findItem(R.id.checkmark).setVisible(false);
        menu.findItem(R.id.ble_scan_stop).setVisible(true);
        if(scanState == ScanState.LESCAN) {
            leScanStopHandler.postDelayed(this::stopScan, LESCAN_PERIOD);
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void[] params) {
                    bluetoothAdapter.startLeScan(null, leScanCallback);
                    return null;
                }
            }.execute(); // start async to prevent blocking UI, because startLeScan sometimes take some seconds
        } else {
            bluetoothAdapter.startDiscovery();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // ignore requestCode as there is only one in this fragment
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            new Handler(Looper.getMainLooper()).postDelayed(this::startScan,1); // run after onResume to avoid wrong empty-text
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(getText(R.string.location_denied_title));
            builder.setMessage(getText(R.string.location_denied_message));
            builder.setPositiveButton(android.R.string.ok, null);
            builder.show();
        }
    }

    private void updateScan(BluetoothDevice device) {
        if(scanState == ScanState.NONE)
            return;
        if(listItems.indexOf(device) < 0) {
            listItems.add(device);
            Collections.sort(listItems, DevicesFragment::compareTo);
            listAdapter.notifyDataSetChanged();
        }
    }

    private void stopScan() {
        if(scanState == ScanState.NONE)
            return;
        setEmptyText("<no bluetooth devices found>");
        if(menu != null) {
            menu.findItem(R.id.checkmark).setVisible(true);
            menu.findItem(R.id.ble_scan_stop).setVisible(false);
        }
        switch(scanState) {
            case LESCAN:
                leScanStopHandler.removeCallbacks(this::stopScan);
                bluetoothAdapter.stopLeScan(leScanCallback);
                break;
            case DISCOVERY:
                bluetoothAdapter.cancelDiscovery();
                break;
            default:
                // already canceled
        }
        scanState = ScanState.NONE;

    }

    public static Fragment fragment1;
    public static Fragment fragment2;
    public static Fragment fragment3;
    public static Fragment fragment4;
    public static Fragment fragment5;

    int mSelectedItem = -1;

    public static String[] bleDeviceName =new String[5];

    public static int index = -1;

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        stopScan();
        mSelectedItem = position;
        listAdapter.notifyDataSetChanged();
        BluetoothDevice device = listItems.get(position-1);
        Bundle args = new Bundle();
        args.putString("device", device.getAddress());
        index = Arrays.asList(bleDeviceName).indexOf(device.getName());
        boolean connect = false;
        if(index == -1) {
            if (fragment1 == null && !connect && device.getName().contains("EVT-TEMPSENSE-F5DD")) {
                fragment1 = new TerminalFragment1();
                fragment1.setArguments(args);
                getFragmentManager().beginTransaction().replace(R.id.fragment1, fragment1, "terminal").addToBackStack(null).commit();
                bleDeviceName[0] = device.getName();
                FragmentOne.fragment1.bringToFront();
                FragmentTwo.chart1.getDescription().setText(device.getName());
                connect = true;
            }
            if (fragment2 == null && !connect && device.getName().contains("EVT-TEMPSENSE-BEE9")) {
                fragment2 = new TerminalFragment2();
                fragment2.setArguments(args);
                getFragmentManager().beginTransaction().replace(R.id.fragment2, fragment2, "terminal2").addToBackStack(null).commit();
                bleDeviceName[1] = device.getName();
                FragmentOne.fragment2.bringToFront();
                FragmentTwo.chart2.getDescription().setText(device.getName());
                connect = true;
            }
            if (fragment3 == null&& !connect) {
                fragment3 = new TerminalFragment3();
                fragment3.setArguments(args);
                getFragmentManager().beginTransaction().replace(R.id.fragment3, fragment3, "terminal3").addToBackStack(null).commit();
                bleDeviceName[2] = device.getName();
                FragmentOne.fragment3.bringToFront();
                FragmentTwo.chart3.getDescription().setText(device.getName());
                connect = true;
            }
            if (fragment4 == null&& !connect) {
                fragment4 = new TerminalFragment4();
                fragment4.setArguments(args);
                getFragmentManager().beginTransaction().replace(R.id.fragment4, fragment4, "terminal4").addToBackStack(null).commit();
                bleDeviceName[3] = device.getName();
                FragmentOne.fragment4.bringToFront();
                FragmentTwo.chart4.getDescription().setText(device.getName());
                connect = true;
            }
            if (fragment5 == null&& !connect) {
                fragment5 = new TerminalFragment5();
                fragment5.setArguments(args);
                getFragmentManager().beginTransaction().replace(R.id.fragment5, fragment5, "terminal5").addToBackStack(null).commit();
                bleDeviceName[4] = device.getName();
                FragmentOne.fragment5.bringToFront();
                FragmentTwo.chart5.getDescription().setText(device.getName());
                connect = true;
            }
        }
        index = Arrays.asList(bleDeviceName).indexOf(device.getName());
        if(index == 0)
        {
            FragmentOne.fragment1.bringToFront();
            if (fragment1 == null) {
                fragment1 = new TerminalFragment1();
                fragment1.setArguments(args);
                getFragmentManager().beginTransaction().replace(R.id.fragment1, fragment1, "terminal").addToBackStack(null).commit();
            }
        }
        else if(index == 1)
        {
            FragmentOne.fragment2.bringToFront();
            if (fragment2 == null) {
                fragment2 = new TerminalFragment2();
                fragment2.setArguments(args);
                getFragmentManager().beginTransaction().replace(R.id.fragment2, fragment2, "terminal2").addToBackStack(null).commit();
            }
        }
        else if(index == 2)
        {
            FragmentOne.fragment3.bringToFront();
            if (fragment3 == null) {
                fragment3 = new TerminalFragment3();
                fragment3.setArguments(args);
                getFragmentManager().beginTransaction().replace(R.id.fragment3, fragment3, "terminal3").addToBackStack(null).commit();
            }
        }
        else if(index == 3)
        {
            FragmentOne.fragment4.bringToFront();
            if (fragment4 == null) {
                fragment4 = new TerminalFragment4();
                fragment4.setArguments(args);
                getFragmentManager().beginTransaction().replace(R.id.fragment4, fragment4, "terminal4").addToBackStack(null).commit();
            }
        }
        else if(index == 4)
        {
            FragmentOne.fragment5.bringToFront();
            if (fragment5 == null) {
                fragment5 = new TerminalFragment5();
                fragment5.setArguments(args);
                getFragmentManager().beginTransaction().replace(R.id.fragment4, fragment4, "terminal4").addToBackStack(null).commit();
            }
        }
    }

    /**
     * sort by name, then address. sort named devices first
     */
    static int compareTo(BluetoothDevice a, BluetoothDevice b) {
        boolean aValid = a.getName()!=null && !a.getName().isEmpty();
        boolean bValid = b.getName()!=null && !b.getName().isEmpty();
        if(aValid && bValid) {
            int ret = a.getName().compareTo(b.getName());
            if (ret != 0) return ret;
            return a.getAddress().compareTo(b.getAddress());
        }
        if(aValid) return -1;
        if(bValid) return +1;
        return a.getAddress().compareTo(b.getAddress());
    }
}
